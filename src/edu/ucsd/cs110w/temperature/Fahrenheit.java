/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author janz
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return getValue() + " F";
	}
	@Override
	public Temperature toCelsius() {
		float temp = getValue();
		temp = (float) ((temp - 32) * (5.0/9));
		Celsius c = new Celsius(temp);
		return c;
	}
	@Override
	public Temperature toFahrenheit() {
		float temp = getValue();
		Fahrenheit f = new Fahrenheit(temp);
		return f;
	}
	public Temperature toKelvin()
	{
		return null;
	}
}
