/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author achinn
 *
 */
public class Kelvin extends Temperature {	
	public Kelvin(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return getValue() + " K";
	}
	@Override
	public Temperature toCelsius() {
		float temp = getValue();
		temp = (float) (temp + 273);
		Celsius c = new Celsius(temp);
		return c;
	}
	@Override
	public Temperature toFahrenheit() {
		float temp = getValue();
		temp = (float) (temp + 273);
		temp = (float)((temp*9)/5) + 32;
		Fahrenheit f = new Fahrenheit(temp);
		return f;
	}
	
	public Temperature toKelvin()
	{
		float temp = getValue();
		Kelvin k = new Kelvin(temp);
		return k;
	}

}
