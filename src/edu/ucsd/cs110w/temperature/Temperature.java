/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author janz
 *
 */

public abstract class Temperature
{
   float value;
   public Temperature(float v)
   {
      value = v;
   }
   public final float getValue()
   {
      return value;
   }
   public abstract Temperature toCelsius();

   public abstract Temperature toFahrenheit();
   
   public abstract Temperature toKelvin();
}
