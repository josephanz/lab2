/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author janz
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
	   super(t);
	} 
	public String toString()
	{

		return getValue()+ " C";	
	}
	@Override
	public Temperature toCelsius() {
		float temp = getValue();
		Celsius c = new Celsius(temp);
		return c;
	}
	@Override
	public Temperature toFahrenheit() {
		float temp = getValue();
		temp = (float)((temp*9)/5) + 32;
		Fahrenheit f = new Fahrenheit(temp);
		return f;
	}
	public Temperature toKelvin()
	{
		return null;
	}
}
